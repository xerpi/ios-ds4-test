#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "conf.h"
#include "ipc.h"
#include "mem.h"
#include "syscalls.h"
#include "timer.h"
#include "tools.h"
#include "types.h"
#include "utils.h"
#include "usb2.h"

/* IOCTL commands */
#define USBV5_IOCTL_GETVERSION                   0
#define USBV5_IOCTL_GETDEVICECHANGE              1
#define USBV5_IOCTL_SHUTDOWN                     2
#define USBV5_IOCTL_GETDEVPARAMS                 3
#define USBV5_IOCTL_ATTACHFINISH                 6
#define USBV5_IOCTL_SETALTERNATE                 7
#define USBV5_IOCTL_SUSPEND_RESUME              16
#define USBV5_IOCTL_CANCELENDPOINT              17
#define USBV5_IOCTL_CTRLMSG                     18
#define USBV5_IOCTL_INTRMSG                     19
#define USBV5_IOCTL_ISOMSG                      20
#define USBV5_IOCTL_BULKMSG                     21

/* Constants */
#define USB_MAX_DEVICES		32

/* Notification messages */
static areply notification_messages[2] = {{ -1, -1 }};
#define MESSAGE_DEVCHANGE	&notification_messages[0]
#define MESSAGE_ATTACHFINISH	&notification_messages[1]

static u8 heapspace[4096] ATTRIBUTE_ALIGN(32);

char *moduleName = "TST";
static u32 queue_data[32] ATTRIBUTE_ALIGN(32);
static s32 queue_id = -1;

#define PERIODC_TIMER_PERIOD	250 * 1000
static int periodic_timer_id;
static int periodic_timer_cookie;


int main(void)
{
	int ret;
	int fd;
	u32 ver[8] ATTRIBUTE_ALIGN(32);
	usb_device_entry attached_devices[USB_MAX_DEVICES] ATTRIBUTE_ALIGN(32);

	/* Print info */
	printf("Hello world from Starlet!\n");

	ret = Mem_Init(heapspace, sizeof(heapspace));
	if (ret < 0)
		return ret;

	ret = Timer_Init();
	if (ret < 0)
		return ret;

	/* Create message queue */
	ret = os_message_queue_create(queue_data, ARRAY_SIZE(queue_data));
	if (ret < 0)
		return ret;
	queue_id = ret;

	periodic_timer_id = os_create_timer(PERIODC_TIMER_PERIOD, PERIODC_TIMER_PERIOD,
					    queue_id, (u32)&periodic_timer_cookie);
	if (ret < 0)
		return ret;

	fd = os_open("/dev/usb/ven", 0);
	if (fd < 0)
		return IOS_ENOENT;
	printf("/dev/usb/hid: %d\n", ret);

	//Timer_Sleep(1000 * 1000);

	void *buf = Mem_Alloc(0x600);
	bool is_dev_change = false;

	ret = os_ioctl(fd, USBV5_IOCTL_GETVERSION, NULL, 0, ver, sizeof(ver));
	printf("GETVERSION (%d): 0x%x\n", ret, ver[0]);

	/* Get device changes */
	ret = os_ioctl_async(fd, USBV5_IOCTL_GETDEVICECHANGE, NULL, 0, buf,
			     sizeof(attached_devices), queue_id, MESSAGE_DEVCHANGE);
	printf("GETDEVICECHANGE (%d)\n", ret);
	if (ret == IOS_OK)
		is_dev_change = true;

	//USB_GetDeviceList(usb_device_entry *buffer, u8 num_descr, u8 interface, u8 *cnt_descr);

	/* Main loop */
	while (1) {
		u32 message;

		/* Wait for message */
		ret = os_message_queue_receive(queue_id, (void *)&message, 0);
		if (ret != IOS_OK)
			continue;

		if (message == (u32)&periodic_timer_cookie) {
			//printf("Timer!\n");
			if (!is_dev_change) {
				ret = os_ioctl_async(fd, USBV5_IOCTL_GETDEVICECHANGE, NULL, 0, buf,
						     sizeof(attached_devices), queue_id, MESSAGE_DEVCHANGE);
				printf("GETDEVICECHANGE (%d)\n", ret);
				if (ret == IOS_OK)
					is_dev_change = true;
			}
		} else if (message == (u32)MESSAGE_DEVCHANGE) {
			areply *msg = (areply *)message;
			printf("Got device change: %d\n", msg->result);
			if (msg->result >= 0) {
				for (int i = 0; i < msg->result; i++) {
					printf("[%d] VID: 0x%04x, PID: 0x%04x", i,
						attached_devices[i].vid,
						attached_devices[i].pid);
				}

				ret = os_ioctl_async(fd, USBV5_IOCTL_ATTACHFINISH, NULL, 0, NULL, 0,
						     queue_id, MESSAGE_ATTACHFINISH);
				printf("ATTACHFINISH (%d)\n", ret);
			}
			is_dev_change = false;
		} else if (message == (u32)MESSAGE_ATTACHFINISH) {
			printf("Attach finish!\n");
			ret = os_ioctl_async(fd, USBV5_IOCTL_GETDEVICECHANGE, NULL, 0, buf,
					     sizeof(attached_devices), queue_id, MESSAGE_DEVCHANGE);
			if (ret == IOS_OK)
				is_dev_change = true;
			printf("GETDEVICECHANGE (%d)\n", ret);
		}
	}

	os_close(fd);

	return 0;
}

void my_assert_func(const char *file, int line, const char *func, const char *failedexpr)
{
	printf("assertion \"%s\" failed: file \"%s\", line %d%s%s\n",
		failedexpr, file, line, func ? ", function: " : "", func ? func : "");
}
