#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "conf.h"
#include "ipc.h"
#include "mem.h"
#include "syscalls.h"
#include "timer.h"
#include "tools.h"
#include "types.h"
#include "utils.h"
#include "usb.h"

#define DS4_VID 0x054C
#define DS4_PID 0x05C4

/* IOCTL commands */
#define USBV5_IOCTL_GETVERSION                   0
#define USBV5_IOCTL_GETDEVICECHANGE              1
#define USBV5_IOCTL_SHUTDOWN                     2
#define USBV5_IOCTL_GETDEVPARAMS                 3
#define USBV5_IOCTL_ATTACH                       4
#define USBV5_IOCTL_RELEASE                      5
#define USBV5_IOCTL_ATTACHFINISH                 6
#define USBV5_IOCTL_SETALTERNATE                 7
#define USBV5_IOCTL_SUSPEND_RESUME              16
#define USBV5_IOCTL_CANCELENDPOINT              17
#define USBV5_IOCTL_CTRLMSG                     18
#define USBV5_IOCTL_INTRMSG                     19
#define USBV5_IOCTL_ISOMSG                      20
#define USBV5_IOCTL_BULKMSG                     21

/* Constants */
#define USB_MAX_DEVICES		32

char *moduleName = "TST";

/* Notification messages */
static areply notification_messages[2] = {0};
#define MESSAGE_DEVCHANGE	&notification_messages[0]
#define MESSAGE_ATTACHFINISH	&notification_messages[1]

static u8 heapspace[4096] ATTRIBUTE_ALIGN(32);
static u32 queue_data[32] ATTRIBUTE_ALIGN(32);
static int queue_id = -1;

int USB_GetDescriptors(int host_fd, u32 dev_id, usb_devdesc *udd)
{
	u32 *iobuf  = NULL;
	u8  *buffer = NULL, *next;

	usb_configurationdesc *ucd = NULL;
	usb_interfacedesc     *uid = NULL;
	usb_endpointdesc      *ued = NULL;

	u32 iConf, iEndpoint;
	int ret = IOS_ENOMEM;

	/* Allocate memory */
	iobuf  = Mem_Alloc(0x20);
	buffer = Mem_Alloc(0x60);

	/* Error */
	if (!iobuf || !buffer)
		goto out;

	/* Setup buffer */
	iobuf[0] = dev_id;
	iobuf[2] = 0;

	/* Get device parameters */
	ret = os_ioctl(host_fd, USBV5_IOCTL_GETDEVPARAMS, iobuf, 0x20, buffer, 0x60);
	if (ret)
		goto out;

	next = buffer + 20;

	/* Copy descriptors */
	memcpy(udd, next, sizeof(*udd));

	/* Allocate memory */
	udd->configurations = Mem_Alloc(udd->bNumConfigurations * sizeof(*udd->configurations));
	if (!udd->configurations)
		goto out;

	next += (udd->bLength + 3) & ~3;

	for (iConf = 0; iConf < udd->bNumConfigurations; iConf++) {
		ucd = &udd->configurations[iConf];
		memcpy(ucd, next, USB_DT_CONFIG_SIZE);
		next += (USB_DT_CONFIG_SIZE+3)&~3;

		if (ucd->bNumInterfaces)
			ucd->bNumInterfaces = 1;

		/* Allocate buffer */
		ucd->interfaces = Mem_Alloc(ucd->bNumInterfaces * sizeof(*ucd->interfaces));
		if (!ucd->interfaces)
			goto out;

		uid = ucd->interfaces;
		memcpy(uid, next, USB_DT_INTERFACE_SIZE);
		next += (uid->bLength + 3) & ~3;

		/* Allocate buffer */
		uid->endpoints = Mem_Alloc(uid->bNumEndpoints * sizeof(*uid->endpoints));
		if (!uid->endpoints)
			goto out;

		memset(uid->endpoints,0,uid->bNumEndpoints*sizeof(*uid->endpoints));

		for(iEndpoint = 0; iEndpoint < uid->bNumEndpoints; iEndpoint++) {
			ued = &uid->endpoints[iEndpoint];
			memcpy(ued, next, USB_DT_ENDPOINT_SIZE);
			next += (ued->bLength + 3) & ~3;
		}

		/* Success */
		ret = 0;
	}

out:
	if (iobuf)
		Mem_Free(iobuf);
	if (buffer)
		Mem_Free(buffer);

	return ret;
}

static inline int __usb_interrupt_message(int host_fd, int dev_id, int out, u16 wLength, void *rpData)
{
	u32 buf[16] ATTRIBUTE_ALIGN(32);
	ioctlv vectors[2];

	memset(buf, 0, sizeof(buf));
	buf[0] = dev_id;
	buf[2] = out;

	vectors[0].data = buf;
	vectors[0].len  = sizeof(buf);
	vectors[1].data = rpData;
	vectors[1].len  = wLength;

	return os_ioctlv(host_fd, USBV5_IOCTL_INTRMSG, 1 - out, 1 + out, vectors);
}

int USB_ReadIntrMsg(int host_fd, int dev_id, u16 wLength, void *rpData)
{
	return __usb_interrupt_message(host_fd, dev_id, 0, wLength, rpData);
}

int USB_WriteIntrMsg(int host_fd, int dev_id, u16 wLength, void *rpData)
{
	return __usb_interrupt_message(host_fd, dev_id, 1, wLength, rpData);
}

int USB_Attach(int host_fd, u32 dev_id)
{
	u32 buf[8] ATTRIBUTE_ALIGN(32);

	memset(buf, 0, sizeof(buf));
	buf[0] = dev_id;

	return os_ioctl(host_fd, USBV5_IOCTL_ATTACH, buf, sizeof(buf), NULL, 0);
}

int USB_Release(int host_fd, u32 dev_id)
{
	u32 buf[8] ATTRIBUTE_ALIGN(32);

	memset(buf, 0, sizeof(buf));
	buf[0] = dev_id;

	return os_ioctl(host_fd, USBV5_IOCTL_RELEASE, buf, sizeof(buf), NULL, 0);
}

int USB_SuspendResume(int host_fd, int dev_id, int resumed, u32 unk)
{
	u32 buf[8] ATTRIBUTE_ALIGN(32);

	memset(buf, 0, sizeof(buf));
	buf[0] = dev_id;
	buf[2] = unk;
	*((u8 *)buf + 0xb) = resumed;

	return os_ioctl(host_fd, USBV5_IOCTL_SUSPEND_RESUME, buf, sizeof(buf), NULL, 0);
}

static int ds4_set_leds_rumble(int host_fd, int dev_id, u8 r, u8 g, u8 b)
{
	u8 buf[] ATTRIBUTE_ALIGN(32) = {
		0x05, // Report ID
		0x03, 0x00, 0x00,
		0x00, // Fast motor
		0x00, // Slow motor
		r, g, b, // RGB
		0x00, // LED on duration
		0x00  // LED off duration
	};

	return USB_WriteIntrMsg(host_fd, dev_id, sizeof(buf), buf);
}

static int ds4_get_data(int host_fd, int dev_id, void *data, u32 size)
{
	return USB_ReadIntrMsg(host_fd, dev_id, size, data);
}

static int ds4_init(int host_fd, u32 dev_id)
{
	u8 data[128] ATTRIBUTE_ALIGN(32);

	int ret;

	printf("DS4 init %d 0x%08x\n", host_fd, dev_id);

	usb_devdesc udd;
	ret = USB_GetDescriptors(host_fd, dev_id, &udd);
	printf("USB_GetDescriptors: %d\n", ret);

	ret = ds4_set_leds_rumble(host_fd, dev_id, 0, 0, 255);
	printf("ds4_set_leds_rumble: %d\n", ret);

	while (1) {
		ret = ds4_get_data(host_fd, dev_id, data, sizeof(data));
		if (ret > 0) {
			printf("%d %08x %08x %08x\r\n", ret,
				*(u32 *)data, *(u32 *)(data + 4), *(u32 *)(data + 8));
		}
		Timer_Sleep(10 * 1000);
	}

	return 0;
}

int main(void)
{
	int ret;
	int fd;
	u32 ver[8] ATTRIBUTE_ALIGN(32);
	usb_device_entry attached_devices[USB_MAX_DEVICES] ATTRIBUTE_ALIGN(32);

	/* Print info */
	printf("Hello world from Starlet!\n\n");

	ret = Mem_Init(heapspace, sizeof(heapspace));
	if (ret < 0)
		return ret;

	ret = Timer_Init();
	if (ret < 0)
		return ret;

	ret = os_message_queue_create(queue_data, ARRAY_SIZE(queue_data));
	if (ret < 0)
		return ret;
	queue_id = ret;

	fd = os_open("/dev/usb/hid", 0);
	printf("open(/dev/usb/hid): %d\n", fd);
	if (fd < 0)
		return IOS_ENOENT;

	ret = os_ioctl(fd, USBV5_IOCTL_GETVERSION, NULL, 0, ver, sizeof(ver));
	if (ret < 0)
		return ret;
	printf("USB module version: 0x%x\n", ver[0]);

	ret = os_ioctl_async(fd, USBV5_IOCTL_GETDEVICECHANGE, NULL, 0, attached_devices,
			     sizeof(attached_devices), queue_id, MESSAGE_DEVCHANGE);

	bool found = false;
	u32 devid = 0;

	/* Main loop */
	while (1) {
		areply *message;

		/* Wait for message */
		ret = os_message_queue_receive(queue_id, (void *)&message, 0);
		if (ret != IOS_OK)
			continue;

		if (message == MESSAGE_DEVCHANGE) {
			printf("Got device change: %ld %ld\n", message->result, message->unk);
			if (message->result >= 0) {
				for (int i = 0; i < message->result; i++) {
					printf("[%d] VID: 0x%04x, PID: 0x%04x, dev_id: 0x%lx, token: 0x%x\n", i,
						attached_devices[i].vid,
						attached_devices[i].pid,
						attached_devices[i].device_id,
						attached_devices[i].token);
					if ((attached_devices[i].vid == DS4_VID) &&
					    (attached_devices[i].pid == DS4_PID)) {
						found = true;
						devid = attached_devices[i].device_id;

						ret = USB_Attach(fd, devid);
						printf("USB_Attach: %d\n\n", ret);

						ret = os_ioctl_async(fd, USBV5_IOCTL_ATTACHFINISH, NULL, 0, NULL, 0, queue_id, MESSAGE_ATTACHFINISH);
						printf("ATTACHFINISH: %d\n\n", ret);
					}
				}
			}

		} else if (message == MESSAGE_ATTACHFINISH) {
			ret = os_ioctl_async(fd, USBV5_IOCTL_GETDEVICECHANGE, NULL, 0, attached_devices,
					     sizeof(attached_devices), queue_id, MESSAGE_DEVCHANGE);
			if (found) {
				ret = USB_SuspendResume(fd, devid, 1, 0);
				printf("SuspendResume: %d\n", ret);

				ds4_init(fd, devid);
			}

		}
	}

	os_close(fd);

	return 0;
}

void my_assert_func(const char *file, int line, const char *func, const char *failedexpr)
{
	printf("assertion \"%s\" failed: file \"%s\", line %d%s%s\n",
		failedexpr, file, line, func ? ", function: " : "", func ? func : "");
}
