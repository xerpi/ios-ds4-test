#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "conf.h"
#include "ipc.h"
#include "mem.h"
#include "syscalls.h"
#include "timer.h"
#include "tools.h"
#include "types.h"
#include "utils.h"
#include "usb2.h"

/* Device name */
#define DEVICE					"/dev/usb2"
/* USB IOCTL commands */
#define USB_IOCTL_CTRLMSG			0
#define USB_IOCTL_BLKMSG			1
#define USB_IOCTL_INTRMSG			2
#define USB_IOCTL_SUSPENDDEV			5
#define USB_IOCTL_RESUMEDEV			6
#define USB_IOCTL_GETDEVLIST			12
#define USB_IOCTL_DEVREMOVALHOOK		26
#define USB_IOCTL_DEVINSERTHOOK			27

/* Constants */
#define USB_MAX_DEVICES		32
#define PERIODC_TIMER_PERIOD	250 * 1000

/* Notification messages */
static areply notification_messages[2] = {{ -1, -1 }};
#define MESSAGE_DEVCHANGE	&notification_messages[0]
#define MESSAGE_ATTACHFINISH	&notification_messages[1]

static u8 heapspace[4096] ATTRIBUTE_ALIGN(32);

char *moduleName = "TST";
static u32 queue_data[32] ATTRIBUTE_ALIGN(32);
static s32 queue_id = -1;
/* Periodic timer with large period to tick fakedevices to check their state */
static int periodic_timer_id;
static int periodic_timer_cookie;

struct dev {
	u16 unk0;
	u16 unk1;
	u16 vid;
	u16 pid;
};

static int get_device_list(int fd)
{
	struct dev devices[32] ATTRIBUTE_ALIGN(32);
	int ret;

	u8 maxdev ATTRIBUTE_ALIGN(32) = ARRAY_SIZE(devices);
	u8 b0 ATTRIBUTE_ALIGN(32)= 0;
	u8 num ATTRIBUTE_ALIGN(32)= 0;
	ioctlv vector[4] ATTRIBUTE_ALIGN(32)= {
		{&maxdev, sizeof(maxdev)},
		{&b0,     sizeof(b0)},
		{&num,    sizeof(num)},
		{devices, sizeof(devices)},
	};

	ret = os_ioctlv(fd, USB_IOCTL_GETDEVLIST, 2, 2, vector);
	printf("USB_IOCTL_GETDEVLIST: %d\n", ret);

	printf ("num: %d, bo: %d, maxdev: %d\n", num, b0, maxdev);

	return ret;
}

int main(void)
{
	int ret;
	int fd;
	u32 ver[8] ATTRIBUTE_ALIGN(32);
	//usb_device_entry attached_devices[USB_MAX_DEVICES] ATTRIBUTE_ALIGN(32);

	/* Print info */
	printf("Hello world from Starlet!\n");

	ret = Mem_Init(heapspace, sizeof(heapspace));
	if (ret < 0)
		return ret;

	ret = Timer_Init();
	if (ret < 0)
		return ret;

	/* Create message queue */
	ret = os_message_queue_create(queue_data, ARRAY_SIZE(queue_data));
	if (ret < 0)
		return ret;
	queue_id = ret;

	periodic_timer_id = os_create_timer(PERIODC_TIMER_PERIOD, PERIODC_TIMER_PERIOD,
					    queue_id, (u32)&periodic_timer_cookie);
	if (ret < 0)
		return ret;

	//Timer_Sleep(1000 * 1000);

	fd = os_open(DEVICE, 0);
	if (fd < 0)
		return IOS_ENOENT;
	printf(DEVICE ": %d\n", ret);

#if 0
int ehci_get_device_list(u8 maxdev,u8 b0,u8*num,u16*buf)
{
        int i,j = 0;
        for(i=0;i<ehci->num_port && j<maxdev ;i++)
        {
                struct ehci_device *dev = &ehci->devices[i];
                if(dev->id != 0){
                        //ehci_dbg ( "device %d: %X %X...\n", dev->id,le16_to_cpu(dev->desc.idVendor),le16_to_cpu(dev->desc.idProduct));
                        buf[j*4] = 0;
                        buf[j*4+1] = 0;
                        buf[j*4+2] = le16_to_cpu(dev->desc.idVendor);
                        buf[j*4+3] = le16_to_cpu(dev->desc.idProduct);
                        j++;
                }

	case USB_IOCTL_GETDEVLIST: {
		u8  maxdev = *(u8 *)vector[0].data;
		u8  b0     = *(u8 *)vector[1].data;
		u8 *num    =  (u8 *)vector[2].data;

		void *buffer = vector[3].data;

		if (!dev)
			ret = ehci_get_device_list(maxdev, b0, num, buffer);
#endif

	get_device_list(fd);

	/* Main loop */
	while (1) {
		u32 message;

		/* Wait for message */
		ret = os_message_queue_receive(queue_id, (void *)&message, 0);
		if (ret != IOS_OK)
			continue;

		if (message == (u32)&periodic_timer_cookie) {
			//get_device_list(fd);
		}
	}

	os_close(fd);

	return 0;
}

void my_assert_func(const char *file, int line, const char *func, const char *failedexpr)
{
	printf("assertion \"%s\" failed: file \"%s\", line %d%s%s\n",
		failedexpr, file, line, func ? ", function: " : "", func ? func : "");
}
