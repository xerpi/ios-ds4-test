#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>

#define bswap16 __builtin_bswap16
#define le16toh bswap16
#define htole16 bswap16

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#define ROUNDUP32(x)	(((u32)(x) + 0x1f) & ~0x1f)
#define ROUNDDOWN32(x)	(((u32)(x) - 0x1f) & ~0x1f)

#ifdef assert
#undef assert
#endif
#define assert(exp) ( (exp) ? (void)0 : my_assert_func(__FILE__, __LINE__, __FUNCTION__, #exp))

#define DEBUG(...) printf(__VA_ARGS__)
//#define DEBUG(...) (void)0

extern void my_assert_func(const char *file, int line, const char *func, const char *failedexpr);


#endif
